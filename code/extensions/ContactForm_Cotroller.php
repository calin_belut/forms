<?php

class ContactForm_Controller extends DataExtension {

    private static $allowed_actions = array('ContactForm');

    // Template method
    public function ContactForm() {
        //die(print_r($this -> owner -> request -> postVars()));
        $fields = new FieldList();

        $fields -> add(new TextField("Name", _t("ContactForm.NAME", "Nome")));
        $fields -> add(new EmailField("Email", _t("ContactForm.EMAIL", "Email")));
        $fields -> add(new TextField("Phone", _t("ContactForm.TELEPHONE", "Telefono")));
        $fields -> add(new TextareaField("Message", _t("ContactForm.MESSAGE", "Messagio")));
        $privacy = new CheckboxField("Privacy", _t("ContactForm.PRIVACY", "<a href='/privacy'>I Accept The Terms and Privacy Policy</a>"));
        $fields -> add($privacy);
        $actions = new FieldList(FormAction::create("sendContactForm") -> setTitle(_t("SEND_CONTACT", "Send")));
        $validators = new RequiredFields('Name', 'Email', 'Privacy');
        $form = new Form($this -> owner, 'ContactForm', $fields, $actions, $validators);
        // Load the form with previously sent data
        $form -> loadDataFrom($this -> owner -> request -> postVars());
        return $form;
    }

    // public function captcha()
    // {
    // require_once ('recaptchalib.php');
    // $privatekey = "6LcEWfoSAAAAAKgn-h2QtQzQ-PgURQsQxt7NStEQ";
    // ///$resp = new Captcha;
    // $resp = recaptcha_check_answer($privatekey, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);
    //
    // if (!$resp -> is_valid)
    // {
    // // What happens when the CAPTCHA was entered incorrectly
    // //die("The reCAPTCHA wasn't entered correctly. Go back and try it
    // // again." . "(reCAPTCHA said: " . $resp -> error . ")");
    // return FALSE;
    // }
    // else
    // {
    // return true;
    // // Your code here to handle a successful verification
    // }
    // }

    public function sendContactForm($data, Form $form) {
        $config = Config::inst();
        $admin = Permission::get_members_by_permission('ADMIN');
        $emails_admin = "";
        foreach ($admin as $value) {
            $emails_admin[] = $value -> Email;
        }
        $message = "";
        $message .= $config -> EmailText;
        $message .= "<br/>
<strong>Nome:</strong>{$data['Name']}<br />
<strong>Email:</strong>{$data['Email']}<br />
<strong>Telefono:</strong>{$data['Phone']}<br />
<strong>Messaggio:</strong>{$data['Message']}<br />
<strong>Inviato da :</strong>{$data['SentFrom']}<br />
";

        if ($config -> AdminEmails) {
            $email = new Email($data['Email'], $config -> AdminEmails, $config -> EmailSubject . "-" . $data['SentFrom'], $message);
        }
        else {
            $email = new Email($data['Email'], implode(",", $emails_admin), $config -> EmailSubject . "-" . $data['SentFrom'], $message);
        }

        // if ($this -> captcha())
        // {
        if ($email -> send()) {
            //TODO Email sending
            $entry = new ContactFormEntry();
            $form -> saveInto($entry);
            $entry -> write();
            if ($this -> owner -> LandingPage() -> ID > 0) {

                return $this -> owner -> redirect(Director::baseURL() . $this -> owner -> LandingPage() -> URLSegment);

            }
            else {
                // $contact = ContactPage::get() -> First();
                // if ($contact -> LandingPage() -> ID > 0)
                // {
                // return $this -> owner -> redirect(Director::baseURL() . $contact -> LandingPage() -> URLSegment);
                // }
                return $this -> owner -> redirectBack();

            }
        }
        // }
        // else
        // {
        //
        // return $this-> owner -> redirectBack();;
        // }

    }

}
