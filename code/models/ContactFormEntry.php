<?php
/**
 * ContactFormEntry class
 *
 *
 * Copyright (c)  All rights reserved.
 *
 */

class ContactFormEntry extends DataObject {
    static $db = array(
        "Name" => "Varchar(250)",
        "Email" => "Varchar(250)",
        "Phone" => "Varchar(100)",
        "Message" => "TEXT",
        "SentFrom" => "Varchar(250)"
    );

    static $has_many = array();
    static $has_one = array("Owner" => "SiteTree");
    static $many_many = array();
    static $belongs_many_many = array();

    // Model Admin utilties
    static $summary_fields = array(
        "Name" => "Name",
        "Email" => "Email",
    );
    static $searchable_fields = array();
    static $singular_name = 'ContactFormEntry';

    static $plural_name = 'ContactFormEntries';

    // static $default_sort = 'LastEdited DESC'

    // Methods
    function requireDefaultRecords() {
        parent::requireDefaultRecords();
    }

    function getCMSFields() {
        $fields = parent::getCMSFields();
        $fields -> removeByName('OwnerID');

        return $fields;
    }

    function onBeforeWrite() {
        parent::onBeforeWrite();
    }

    function onAfterWrite() {
        parent::onAfterWrite();
    }

    function onBeforeDelete() {
        parent::onBeforeDelete();
    }

    function onAfterDelete() {
        parent::onAfterDelete();
    }

    function forTemplate() {
        return $this -> Title;
    }

    function getCMSFields_forPopup() {
        return parent::getCMSFields();
    }

}
