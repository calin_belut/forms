<?php
class ContactPage extends Page
{

    private static $db = array(
        "Address" => "Varchar(250)",
        "Tel" => "Varchar(250)",
        "Fax" => "Varchar(250)",
        "Email" => "Varchar(250)",
        "MapAddress" => "Varchar(250)",
        "CaptchaPublicKey" => "Varchar(250)",
        "CaptchaPrivateKey" => "Varchar(250)"
    );
    static $icon = "forms/images/contact.png";
    private static $has_one = array(
        "MapMarker" => "Image",
        "MapMarker2" => "Image",
        "AddressIcon" => "Image",
        "TelIcon" => "Image",
        "EmailIcon" => "Image",
    );
    function Map()
    {
        Requirements::javascript("http://maps.googleapis.com/maps/api/js?key=AIzaSyBKR3v70r1rFWmjLn7yAf-tfcq3HMy-f-c&sensor=false");
        $mapDiv = "<div id='map-canvas' ></div>";
        return $mapDiv;
    }

    

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields -> addFieldToTab('Root.Main', new TextField("Address", "Address"));
        $fields -> addFieldToTab('Root.Main', new TextField("Tel", "Telephone"));
        $fields -> addFieldToTab('Root.Main', new TextField("Fax", "Fax"));
        $fields -> addFieldToTab('Root.Main', new TextField("Email", "Email"));
        $fields -> addFieldToTab('Root.Map', new TextField("MapAddress", "Map Address"));
        $fields -> removeByName("Content");
        $fields -> addFieldToTab('Root.Map', new UploadField("MapMarker"));
        $fields -> addFieldToTab('Root.Map', new UploadField("MapMarker2"));
        $fields -> addFieldToTab('Root.Main', new UploadField("AddressIcon"));
        $fields -> addFieldToTab('Root.Main', new UploadField("TelIcon"));
        $fields -> addFieldToTab('Root.Main', new UploadField("EmailIcon"));
        $fields -> addFieldToTab('Root.Main', new TextField("CaptchaPublicKey", "CaptchaPublicKey"));
        $fields -> addFieldToTab('Root.Main', new TextField("CaptchaPrivateKey", "CaptchaPrivateKey"));
        return $fields;
    }

}

class ContactPage_Controller extends Page_Controller
{

}
