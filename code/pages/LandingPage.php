<?php
/**
 * LandingPage class
 *
 *
 * Copyright (c)  All rights reserved.
 *
 */

class LandingPage extends Page
{

    static $db = array(

    );

    static $has_one = array(

	);

    static $has_many = array(

    );
    static $many_many = array(

	);

    static $belongs_many_many = array(

	);

    static $allowed_children = array(

	);

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        return $fields;
    }

    public function getCMSFields_forPopup()
    {
        $fields = parent::getCMSFields();
        return $fields;
    }

}

class LandingPage_Controller extends Page_Controller
{

    public static $allowed_actions = array();

    public function init()
    {
        parent::init();
        // Note: you should use SS template require tags inside your templates
        // instead of putting Requirements calls here.  However these are
        // included so that our older themes still work

    }

}